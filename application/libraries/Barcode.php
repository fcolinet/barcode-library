<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Barcode Class
 *
 * Barcode generator
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Florent Colinet, postmaster@florentcolinet.fr
 */
class CI_Barcode {

	var $barcode_enable		= "";
	var $barcode_default	= "";
	var $CI;

	/**
	 * Constructor - Sets Barcode Preferences
	 *
	 * The constructor can be passed an array of config values
	 */
	public function __construct($config = array())
	{
		log_message('debug', "Barcode Class Initialized");

		// Set the super object to a local variable for use throughout the class
		$this->CI =& get_instance();

		foreach (array('barcode_default') as $key)
		{
			$this->$key = (isset($config[$key])) ? $config[$key] : $this->CI->config->item($key);
		}

		log_message('debug', "Barcode routines successfully run");
	}

	// --------------------------------------------------------------------

	/**
	 * gd_b64
	 *
	 * @access	public
	 * @return	void
	 */
	function gd_b64($value, $width=500, $height=25, $control=true, $text=false, $coding='')
	{
		if(!isset($value) || $value == '') {
			log_message('debug', "A value must be specified.");
			throw new Exception("A value must be specified.");
		}

		if($coding == '') $coding = $this->barcode_default;
		if($coding == '') $coding = 'barcode39';

		$this->CI->load->library('Barcode/'.$coding);

		try {
			return 'data:image/png;base64,' . $this->CI->$coding->generate($value, $control, $text, $width, $height);
		} catch(Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}

}
// END CI_Barcode class

/* End of file Barcode.php */
/* Location: ./application/libraries/Barcode.php */
