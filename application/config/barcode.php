<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| BarCode parameters
|
| Barcode default encoding :
|     - codabar
|     - barcode11 (code 11)
|     - barcode39 (code 39)
|     - barcode93 (code 93)
|     - barcode128 (code 128)
|     - ean8 (ean 8)
|     - ean13 (ean 13)
|     - std25 (standard 2 of 5 - industrial 2 of 5)
|     - int25 (interleaved 2 of 5)
|     - msi
|
|--------------------------------------------------------------------------
|
*/
$config['barcode_default']	= "barcode39";