# README #

Cette librairie, pour CodeIgniter v2, permet de générer des code barre.

### Installation ###

Copier / coller le dossier 'application' dans CodeIgniter.

Editer le fichier 'application\config\barcode.php'

* Renseigner le codage par défaut dans 'barcode_default'

Editer le fichier 'application\config\autoload.php'

* Dans '$autoload['libraries']', ajouter 'Barcode'
* Dans '$autoload['config']', ajouter 'barcode'

### Utilisation ###

```
#!php
	/**
	 * paramètre 1, caractère : la donnée à encoder
	 * paramètre 2, entier    : largeur
	 * paramètre 3, entier    : hauteur
	 * paramètre 4, booléen   : bit de controle (si le codage le permet)
	 * paramètre 5, booléen   : affichage de la donnée
	 * paramètre 6, caractère : type d'encodage
	 */
	$img = $this->barcode->gd_b64("1234567", 500, 75, true, true, 'barcode39');
```
```
#!html
	<img scr='<?php echo $img; ?>' alt='Code barre codé en base 64' />
```